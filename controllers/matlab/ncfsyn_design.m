%%
close all; clear; clc;
%% Parameters
w = .29; %wheel base
l = ureal('l', .33226, 'Range', [.2598, .4281]); % Rod length
r = .08; % Wheel diameter
m_b = 4; %Body mass TODO: maybe can be also modeled as uncertain
m_w = .3;%ureal('m_w', .3, 'Percentage', 50);
J =  .000735;%ureal('J', 10*.000735, 'Percentage', 50);% Wheel moment of inertia about its turning axis
K = .00039;%ureal('K', 10*.00039, 'Percentage', 50); % Wheel moment of inertia 
%(old;) I_x = 6.57e-2;% Pendulum moment of inertia about X (roll)
%(old) I_y = 6.57e-2;%0118625; % Pendulum moment of inertia about Y (pitch)
%(old) I_z = 9.25e-2;% Pendulum moment of inertia about Z (yaw)
I_x = 4.84e-2;%.015625; % Pendulum moment of inertia about X (roll)
I_y = 3.77e-2;%.0118625;%0118625; % Pendulum moment of inertia about Y (pitch)
I_z = 4.06e-2;%.0118625; % Pendulum moment of inertia about Z (yaw)
b = .01; % Wheel damping %ureal('b', .01, 'Percentage', 50);
g = 9.81;

%% Linearized 3D Segway Model (already reduced...)
d1 = (2*I_y*J + I_y*m_b*r^2 + 2*I_y*m_w*r^2 ...
          + 2*J*l^2*m_b + 2*l^2*m_b*m_w*r^2);

d2 = (r*(2*I_y*J + I_y*m_b*r^2 + 2*I_y*m_w*r^2 ...
          + 2*J*l^2*m_b + 2*l^2*m_b*m_w*r^2));

d3 = (4*K*r^2 + 2*I_z*r^2 + J*w^2 + m_w*r^2*w^2);

a11 = -(2*I_y*b + 2*b*l*m_b*r + 2*b*l^2*m_b)/d1;
a12 =  (2*I_y*b*r + 2*b*l*m_b*r^2 + 2*b*l^2*m_b*r)/d1;
a14 = -(g*l^2*m_b^2*r^2)/d1;

a21 = (4*J*b + 2*b*m_b*r^2 + 4*b*m_w*r^2 + 2*b*l*m_b*r)/d2;
a22 = -(4*J*b*r + 2*b*m_b*r^3 + 4*b*m_w*r^3 + 2*b*l*m_b*r^2)/d2;
a24 = (2*J*g*l*m_b*r + g*l*m_b^2*r^3 + 2*g*l*m_b*m_w*r^3)/d2;

a33 = -(b*w^2)/d3;

Ac = [ a11, a12,  0 ,  a14;
       a21, a22,  0 ,  a24;
        0 ,  0 , a33,   0 ;
        0 ,  1 ,  0 ,   0 ];

b11 = -(I_y*r + l*m_b*r^2 + l^2*m_b*r)/d1;
b12 = b11;

b21 = (2*J*r + m_b*r^3 + 2*m_w*r^3 + l*m_b*r^2)/d2;
b22 = b21;

b31 = (r*w)/d3;
b32 = -b31;

Bc = [ b11, b12;
       b21, b22;
       b31, b32;
        0 ,  0 ];

Cc = eye(size(Ac));
Dc = zeros(size(Bc));

sys = ss(Ac, Bc, Cc, Dc);

% All segway states are measurable
sys.StateName = {'forward velocity';'pitch rate';'yaw rate';'pitch'};
sys.InputName = {'left wheel torque';'right wheel torque'};

G = sys.nom;

%% Loopshaping
s = tf('s');

w11 = 1.5*(s+0.05)/(s);

W1 = blkdiag(w11, w11);

Gs = G*W1;

omega=10;

H = evalfr(Gs,1i*omega);
[U,S,V] = svd(H);
v3 = V(:,2);

%W1 = W1*(eye(2) + real(v3*v3'));

W2 = blkdiag(1,1,1,1);

Gu = 1*eye(2); 

Gn = G*Gu;

Gs = W2*(Gn)*W1;

%% Robustify
[As, Bs, Cs, Ds] = coprimeunc(Gs.A, Gs.B, Gs.C, Gs.D, 1.2);
Ks = ss(As, Bs, Cs, Ds);

% Knc = ncfsyn(-G, W1, W2);

%% 2DOF Test (not working)
% Tr = 2;
% 
% wn = 2*pi;
% 
% zeta = 8 / (Tr * wn);
% 
% num = wn^2;
% den = [1, 2*zeta*wn, wn^2];
% Tref = tf(num, den)*eye(4);
% 
% Tref = minreal(ss(5/(s+5)*eye(4)));
% 
% W0 = eye(4);
% Gt = G*W1;
% K2dof = twoDegressFreedom(Gt, W0*Tref, W0);
% 
% %K2dofs = tf(K2dof);
% tmp = tf(K2dof);
% K1 = tmp(:, 1:4);
% K2 = tmp(:, 5:end);
% 
% Wi = (W0/(eye(4) - dcgain(G)*dcgain(K2))*dcgain(G)*dcgain(K1))\dcgain(Tref);


K = minreal(W1*Ks*W2); % Controller

% Sensitivity functions
L = minreal(sys*(-K));
I = eye(size(L));
S = minreal(feedback(I,L)); 
T = minreal(I-S);
KS = minreal((-K)*S);

% Prefilter
R = 1/(0.2*s);
[mag1, freq1] = sigma(Gs,{10^-3, 10^4});
[mag2, freq2] = sigma(G*K, {10^-3, 10^4});

figure;
semilogx(freq1, mag2db(mag1(1,:)), 'LineWidth', 2, 'Color', '#0072BD');
hold on
semilogx(freq2, mag2db(mag2), 'LineWidth', 2, 'Color', '#D95319');
hold on
semilogx(freq1, mag2db(mag1(2,:)), 'LineWidth', 2, 'Color', '#0072BD');
hold on
semilogx(freq2, mag2db(mag2), 'LineWidth', 2, 'Color', '#D95319');
legend('$\mathbf{W}_2\mathbf{G}\mathbf{W}_1$', '$\mathbf{G}\mathbf{K}$', 'Interpreter', 'Latex', 'FontSize',12);
ylim([-100, 100]);
xlim([10^-3, 10^4])
xlabel('Frequency [rad/s]', 'Interpreter', 'latex', 'FontSize',12);
ylabel('Singular Values [dB]', 'Interpreter', 'latex', 'FontSize',12);
grid minor

pos = get(gcf, 'Position');
set(gcf, 'Position',pos+[-100 0 100 0])

% figure;
% sigma(KS);
% figure;
% sigma(S);
% figure;
% sigma(T);

K0 = dcgain(Ks*W2);

% figure;
% step(T(:, 1))
% xlim([0, 10])
% grid;

W1 = ss(W1);

%% Responses
lp = linspace(l.range(1), l.range(2), 20);
nlp = length(lp);

% for i=1:nlp
%     Ai = usubs(sys.A, 'l', lp(i));
%     Bi = usubs(sys.B, 'l', lp(i));
%     o(i) = sim('analysis.slx');
% end
% hold off
% grid minor

% figure;
% subplot(4,2,1)
% for i=1:nlp
%     plot(o(i).yaw.Time, o(i).yaw.Data(:,3), 'LineWidth', 2, 'Color', '#0072BD');
%     hold on
% end
% ylabel('$\dot{\psi}$[rad/s]', 'Interpreter', 'Latex', 'FontSize',12)
% xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
% hold off
% grid minor;
% 
% subplot(4,2,2)
% for i=1:nlp
%     plot(o(i).vel.Time, o(i).vel.Data(:,3), 'LineWidth', 2, 'Color', '#0072BD');
%     hold on
% end
% ylabel('$\dot{\psi}$[rad/s]', 'Interpreter', 'Latex', 'FontSize',12)
% xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
% hold off
% grid minor;
% 
% subplot(4,2,3)
% for i=1:nlp
%     plot(o(i).yaw.Time, o(i).yaw.Data(:,1), 'LineWidth', 2, 'Color', '#0072BD');
%     hold on
% end
% ylabel('$\dot{x}$[m/s]', 'Interpreter', 'Latex', 'FontSize',12)
% xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
% hold off
% grid minor;
% 
% subplot(4,2,4)
% for i=1:nlp
%     plot(o(i).vel.Time, o(i).vel.Data(:,1), 'LineWidth', 2, 'Color', '#0072BD');
%     hold on
% end
% ylabel('$\dot{x}$[m/s]', 'Interpreter', 'Latex', 'FontSize',12)
% xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
% hold off
% grid minor;
% 
% subplot(4,2,5)
% for i=1:nlp
%     plot(o(i).yaw.Time, o(i).yaw.Data(:,2), 'LineWidth', 2, 'Color', '#0072BD');
%     hold on
% end
% ylabel('$\dot{\phi}$[rad/s]', 'Interpreter', 'Latex', 'FontSize',12)
% xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
% hold off
% grid minor;
% 
% subplot(4,2,6)
% for i=1:nlp
%     plot(o(i).vel.Time, o(i).vel.Data(:,2), 'LineWidth', 2, 'Color', '#0072BD');
%     hold on
% end
% ylabel('$\dot{\phi}$[rad/s]', 'Interpreter', 'Latex', 'FontSize',12)
% xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
% hold off
% grid minor;
% 
% subplot(4,2,7)
% for i=1:nlp
%     plot(o(i).yaw.Time, o(i).yaw.Data(:,4), 'LineWidth', 2, 'Color', '#0072BD');
%     hold on
% end
% ylabel('$\phi$[rad]', 'Interpreter', 'Latex', 'FontSize',12)
% xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
% hold off
% grid minor;
% 
% subplot(4,2,8)
% for i=1:nlp
%     plot(o(i).vel.Time, o(i).vel.Data(:,4), 'LineWidth', 2, 'Color', '#0072BD');
%     hold on
% end
% ylabel('$\phi$[rad]', 'Interpreter', 'Latex', 'FontSize',12)
% xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
% hold off
% grid minor;
% 
% pos = get(gcf, 'Position');
% set(gcf, 'Position',pos+[0 -50 0 50])

%% Hanus Form
W1 = ss(W1.A - W1.B/W1.D*W1.C, [zeros(size(W1.B/W1.D)), W1.B/W1.D], W1.C, [W1.D, zeros(2)]);

% Discretize
W1z = (c2d(W1, .001, 'zoh'));
Kz = c2d(Ks, .001, 'zoh');
Kfz = c2d(K, .001, 'zoh');
%% Save
save('W1.mat', 'W1z');
save('W2.mat', 'W2');
save('Kls.mat', 'Kz');
save('Kfls.mat', 'Kfz');
save('K0.mat', 'K0');