% Uses Control toolbox
function [Ac,Bc,Cc,Dc,gammin]=coprimeunc(a,b,c,d,gamrel)
%%
% Finds the controller which optimally ``robustifies'' a given shaped plant
% in terms of tolerating maximum coprime uncertainty.
%%
% INPUTS:
% % a,b,c,d: State-space description of (shaped) plant.
% % gamrel: gamma used is gamrel*gammin (typical gamrel=1.1)
% %%
% OUTPUTS:
% % Ac,Bc,Cc,Dc: "Robustifying" controller (positive feedback).
% %S
S = eye(size(d'*d))+d'*d;
R = eye(size(d*d'))+d*d';
Rinv = inv(R);Sinv=inv(S);
A1 = (a-b*Sinv*d'*c); R1 = S; B1 = b; Q1 = c'*Rinv*c;
[X,XAMP,G] = care(A1,B1,Q1,R1);
A2 = A1'; Q2 = b*Sinv*b'; B2 = c'; R2 = R;
[Z,ZAMP,G] = care(A2,B2,Q2,R2);
% optimal gamma
XZ = X*Z; gammin = sqrt(1+max(eig(XZ)))
% Use higher gamma
gam = gamrel*gammin; gam2 = gam*gam; gamconst = (1-gam2)*eye(size(XZ));
Lc = gamconst + XZ; Li = inv(Lc'); Fc = -Sinv*(d'*c+b'*X);
Ac = a + b*Fc + gam2*Li*Z*c'*(c+d*Fc);
Bc = gam2*Li*Z*c';
Cc = b'*X;
Dc = -d';
% 
% F = -S^(-1)*(d'*c + b'*X)