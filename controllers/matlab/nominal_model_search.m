%%
close all; clear; clc;
%% Parameters
w = .29; %wheel base
syms l % Rod length
lmax = .4281;
lmin = .2598;
r = .08; % Wheel diameter
m_b = 4; %Body mass
m_w = .3; %Wheel mass
J = .000735; % Wheel moment of inertia about its turning axis
K = .00039;  % Wheel moment of inertia 
% I_x = .015625; % Pendulum moment of inertia about X (roll)
% I_y = .0118625; % Pendulum moment of inertia about Y (pitch)
% I_z = .0118625; % Pendulum moment of inertia about Z (yaw)
I_x = 4.84e-2;%.015625; % Pendulum moment of inertia about X (roll)
I_y = 3.77e-2;%.0118625;%0118625; % Pendulum moment of inertia about Y (pitch)
I_z = 4.06e-2;%.0118625; % Pendulum moment of inertia about Z (yaw)
b = .01; % Wheel damping %ureal('b', .01, 'Percentage', 50);
g = 9.81;

% Model from simscape
%full_model = load('full_model.mat');
%Gfull = full_model.full_model;

%% Linearized 3D Segway Model (already reduced...)
d1 = (2*I_y*J + I_y*m_b*r^2 + 2*I_y*m_w*r^2 ...
          + 2*J*l^2*m_b + 2*l^2*m_b*m_w*r^2);

d2 = (r*(2*I_y*J + I_y*m_b*r^2 + 2*I_y*m_w*r^2 ...
          + 2*J*l^2*m_b + 2*l^2*m_b*m_w*r^2));

d3 = (4*K*r^2 + 2*I_z*r^2 + J*w^2 + m_w*r^2*w^2);

a11 = -(2*I_y*b + 2*b*l*m_b*r + 2*b*l^2*m_b)/d1;
a12 =  (2*I_y*b*r + 2*b*l*m_b*r^2 + 2*b*l^2*m_b*r)/d1;
a14 = -(g*l^2*m_b^2*r^2)/d1;

a21 = (4*J*b + 2*b*m_b*r^2 + 4*b*m_w*r^2 + 2*b*l*m_b*r)/d2;
a22 = -(4*J*b*r + 2*b*m_b*r^3 + 4*b*m_w*r^3 + 2*b*l*m_b*r^2)/d2;
a24 = (2*J*g*l*m_b*r + g*l*m_b^2*r^3 + 2*g*l*m_b*m_w*r^3)/d2;

a33 = -(b*w^2)/d3;

Ac = [ a11, a12,  0 ,  a14;
       a21, a22,  0 ,  a24;
        0 ,  0 , a33,   0 ;
        0 ,  1 ,  0 ,   0 ];

b11 = -(I_y*r + l*m_b*r^2 + l^2*m_b*r)/d1;
b12 = b11;

b21 = (2*J*r + m_b*r^3 + 2*m_w*r^3 + l*m_b*r^2)/d2;
b22 = b21;

b31 = (r*w)/d3;
b32 = -b31;

Bc = [ b11, b12;
       b21, b22;
       b31, b32;
        0 ,  0 ];

%% Griding
np = 5;

glmax = lmax;
glmin = lmin;


while (lmax - lmin) > 1e-2 % Error between max and min height difference in sigmas

ld = linspace(lmin, lmax, np);

[nx, nu] = size(Bc);

A = zeros(nx,nx,np);
B = zeros(nx,nu,np);

T = [ 0  0 -w/r 0;
    -2/r 0   0  0;
      0  1   0  0;
      0  0   0  1];

for i=1:np
    A(:,:,i) = subs(Ac, l, ld(i));
    B(:,:,i) = subs(Bc, l, ld(i));
    Gnom(:,:,i) = ss(A(:,:,i), B(:,:,i), eye(nx), zeros(nx,nu));
end

[~,~,omega] = bode(Gnom(:,:,1));

Gnom_frd = frd(Gnom, omega);

ns = 27;
lds = setdiff(linspace(glmin,glmax,ns), ld);
ns = length(lds);

A = zeros(nx,nx,ns);
B = zeros(nx,nu,ns);
Gi = ss([]);

for i=1:ns
    A(:,:,i) = subs(Ac, l, lds(i));
    B(:,:,i) = subs(Bc, l, lds(i));
    Gi(:,:,i) = ss(A(:,:,i), B(:,:,i), eye(nx), zeros(nx,nu));
end

Gi_frd = frd(Gi, omega) ;
Gfull_frd = frd(Gfull, omega);


for i=1:np
    for j = 1:nx
        G_frd(j,1,:,i) = (Gi_frd(j,1,:) - Gnom_frd(j,1,i))/Gnom_frd(j,1,i);
        %G_frd(j,1,:,i) = (Gfull_frd(j,1) - Gnom_frd(j,1,i))/Gnom_frd(j,1,i);
        %G(j,1,:,i) = tf((Gi(j,1,:) - Gnom(j,1,i))/Gnom(j,1,i));
        % j - out, 1 - in, i - id of nomsys, : - all sys 
    end
end
G = G_frd;
mag = zeros(nx,ns,np,1,length(omega));

for i=1:np
    for j=1:ns
        for k=1:nx
            [ma,~,~] = bode(G(k,1,j,i), omega);
            mag(k,j,i,:) = ma;
        end
    end
end

smag = zeros(ns,np,1,length(omega));

for i=1:np
    for j=1:ns
        [smag(j,i,:), ~] = sigma(G(:,1,j,i), omega);      
    end
end

% for i=1:ns
%     for j = 1:nx
%         [ma,~,~] = bode(G(j,1,i,1), omega);
%         mag(j,i,:) = ma;
%     end
% end

maxm = zeros(nx,np,1,length(omega));
idm = zeros(nx,np,1,length(omega));

for i=1:np
    for j=1:nx
        [maxm(j,i,1,:), idm(j,i,1,:)] = max(mag(j,:,i,:), [], 2);
    end
end

smaxm = zeros(np,1,length(omega));
sidm = zeros(np,1,length(omega));

for i=1:np
    [smaxm(i,1,:), sidm(i,1,:)] = max(smag(:,i,:), [], 1);
end

% figure;
% semilogx(omega, mag2db(squeeze(smaxm(1,1,:))),'LineWidth', 2, 'Color', "#0072BD");
% hold on
% for j=1:ns 
%     semilogx(omega, mag2db(squeeze(smag(j,1,:))), '--', 'Color', "#0072BD");
%     hold on
% end
% semilogx(omega, mag2db(squeeze(smaxm(1,1,:))),'LineWidth', 2, 'Color', "#0072BD");
% grid;
% legend('$e(j\omega)$', 'Interpreter', 'latex', 'FontSize', 15)
% xlabel('Frequency [rad/s]', 'Interpreter', 'latex', 'FontSize', 15);
% ylabel('Singular Values [dB]', 'Interpreter', 'latex', 'FontSize', 15);

[~, id_min] = min(smaxm(:,1,:),[],1);

l_nominal = ld(id_min);

lmax = min(l_nominal(1) + (lmax - l_nominal(1))/2, lmax);
lmin = max(l_nominal(1) - (l_nominal(1) - lmin)/2, lmin);

clear G_frd

end
%%
figure;
legend_info = cell(np,1);
for i=1:np
    semilogx(omega, mag2db(squeeze(smaxm(i,:,:))), 'LineWidth', 2, 'Color', 	"#0072BD");
    %% 
    % f = frd(squeeze(smaxm(i,:,:)), omega);
    % bodemag(f, omega);
    legend_info{i} = ['$\rho$ = ' num2str(ld(i), '%.4f'), ' m'];
    hold on
end
% legend_info{i+1} = ['$\mathcal{W}$'];
freq = flip(omega);
resp_db = flip(mag2db(squeeze(smaxm(end,:,:))));
ord = 2;
wfit
% bodemag(Wtf, 'k--');
hold off
legend(legend_info{1}, 'Interpreter', 'latex', 'FontSize',15);
grid minor;
xlabel('Frequency [rad/s]', 'Interpreter', 'latex', 'FontSize',15);
ylabel('Singular Values [dB]', 'Interpreter', 'latex', 'FontSize',15);
xlim([1e-1, 1e2]);
% bodemag(G(1,1,:,5), omega);
% hold on
% figure;
% subplot(2,2,1);
% for i=1:1
%     semilogx(omega, mag2db(squeeze(maxm(1,i,:,:))), 'LineWidth', 2);
%     legend_info{i} = ['$l_{nom}$ = ' num2str(ld(i))];
%     hold on
% end
% freq = 0;%flip(omega);
% resp_db = 0;%flip(mag2db(squeeze(maxm(1,1,:,:))));
% ord = 2;
% wfit
% W11 = Wtf;%tf([0.3 4e-4], [1 1]);
% bodemag(W11, freq,'--');
% hold off
% %legend(legend_info, 'Interpreter', 'latex');
% grid;
% 
% subplot(2,2,2);
% for i=1:1
%     semilogx(omega, mag2db(squeeze(maxm(2,i,:,:))), 'LineWidth', 2);
%     legend_info{i} = ['$l_{nom}$ = ' num2str(ld(i))];
%     hold on
% end
% freq = flip(omega);
% resp_db = flip(mag2db(squeeze(maxm(2,end,:,:))));
% ord = 2;
% wfit
% W22 = Wtf;
% bodemag(Wtf, freq,'--');
% hold off
% %legend(legend_info, 'Interpreter', 'latex');
% grid;
% 
% subplot(2,2,3);
% for i=1:1
%     semilogx(omega, mag2db(squeeze(maxm(3,i,:,:))), 'LineWidth', 2);
%     legend_info{i} = ['$l_{nom}$ = ' num2str(ld(i))];
%     hold on
% end
% % freq = flip(omega);
% % resp_db = flip(mag2db(squeeze(maxm(3,end,:,:))));
% % ord = 2;
% % wfit
% % bodemag(Wtf, '--');
% hold off
% %legend(legend_info, 'Interpreter', 'latex');
% grid;
% 
% subplot(2,2,4);
% for i=1:1
%     semilogx(omega, mag2db(squeeze(maxm(4,i,:,:))), 'LineWidth', 2);
%     legend_info{i} = ['$l_{nom}$ = ' num2str(ld(i))];
%     hold on
% end
% freq = flip(omega);
% resp_db = flip(mag2db(squeeze(maxm(4,end,:,:))));
% ord = 2;
% wfit
% bodemag(Wtf, freq,'--');
% W44 = Wtf;
% hold off
% %legend(legend_info, 'Interpreter', 'latex');
% grid;


% sys = frd(max_amp, omega);             
% W = fitmagfrd(sys,1);
% Wtf = tf(W);
% hold on
% bodemag(G(1,1,id,1), omega);
% plot(omega, mag2db(max_amp), 'LineWidth', 1);



% re_diff = zeros(np-1, np)
% 
% for i=1:np
%     id = setdiff(1:np, i);
%     sys_frd_id = sys_frd(:,:,id);
%     sys_nom = sys_frd(:,:,i);
%     u2x(1, 1, :, i) = (sys_frd_id(1,1) - sys_nom(1,1))/sys_nom(1,1);
% end

% bodemag(u2x(:,:, omega)


% for i=1:np
%     sysn = tf(sys(:,:,i));
%     figure;
%     idx = setdiff(1:np, i);
%     sigma((tf(sys(:,:,idx)) - tf(sysn))/tf(sysn), sysn);
%     grid;
% end

%% Test
% s = tf('s');
% 
% Gnom = 1/(s+1);
% Gi = 1/(s+1.1);
% 
% [mag, phase, w] = bode(Gnom);
% 
% Gnom_frd = frd(Gnom, w);
% 
% Gi_frd = frd(Gi, w);
% 
% rdiff = (Gi_frd - Gnom_frd)/Gnom_frd;
% 
% figure;
% sigma((Gi - Gnom)/Gnom);
% hold on
% sigma(rdiff, w);



