%%
close all; clear; clc;
%% Parameter for switching between design for real robot or simulator
real_robot = 0; % 1- mean real robot
%% Parameters
w = .29; %wheel base
l = ureal('l', .2907, 'Range', [.2598, .4281]); % Rod length
r = .08; % Wheel diameter
m_b = 4; %Body mass 
m_w = .3; %Wheel mass
J = .000735; % Wheel moment of inertia about its turning axis
K = .00039;  % Wheel moment of inertia 
%(old) I_x = .015625; % Pendulum moment of inertia about X (roll)
%(old) I_y = .0118625; % Pendulum moment of inertia about Y (pitch)
%(old) I_z = .0118625; % Pendulum moment of inertia about Z (yaw)
I_x = 4.84e-2;%.015625; % Pendulum moment of inertia about X (roll)
I_y = 3.77e-2;%.0118625;%0118625; % Pendulum moment of inertia about Y (pitch)
I_z = 4.06e-2;%.0118625; % Pendulum moment of inertia about Z (yaw)
b = .01; % Wheel damping %ureal('b', .01, 'Percentage', 50);
g = 9.81;
h = .001; % Sampling period

%% Linearized 3D Segway Model (already reduced...)
d1 = (2*I_y*J + I_y*m_b*r^2 + 2*I_y*m_w*r^2 ...
          + 2*J*l^2*m_b + 2*l^2*m_b*m_w*r^2);

d2 = (r*(2*I_y*J + I_y*m_b*r^2 + 2*I_y*m_w*r^2 ...
          + 2*J*l^2*m_b + 2*l^2*m_b*m_w*r^2));

d3 = (4*K*r^2 + 2*I_z*r^2 + J*w^2 + m_w*r^2*w^2);

a11 = -(2*I_y*b + 2*b*l*m_b*r + 2*b*l^2*m_b)/d1;
a12 =  (2*I_y*b*r + 2*b*l*m_b*r^2 + 2*b*l^2*m_b*r)/d1;
a14 = -(g*l^2*m_b^2*r^2)/d1;

a21 = (4*J*b + 2*b*m_b*r^2 + 4*b*m_w*r^2 + 2*b*l*m_b*r)/d2;
a22 = -(4*J*b*r + 2*b*m_b*r^3 + 4*b*m_w*r^3 + 2*b*l*m_b*r^2)/d2;
a24 = (2*J*g*l*m_b*r + g*l*m_b^2*r^3 + 2*g*l*m_b*m_w*r^3)/d2;

a33 = -(b*w^2)/d3;

Ac = [ a11, a12,  0 ,  a14;
       a21, a22,  0 ,  a24;
        0 ,  0 , a33,   0 ;
        0 ,  1 ,  0 ,   0 ];

b11 = -(I_y*r + l*m_b*r^2 + l^2*m_b*r)/d1;
b12 = b11;

b21 = (2*J*r + m_b*r^3 + 2*m_w*r^3 + l*m_b*r^2)/d2;
b22 = b21;

b31 = (r*w)/d3;
b32 = -b31;

Bc = [ b11, b12;
       b21, b22;
       b31, b32;
        0 ,  0 ];


Cc = eye(size(Ac));
Dc = zeros(size(Bc));

sys = ss(Ac,Bc,Cc,Dc);

sys.StateName = {'forward velocity';'pitch rate';'yaw rate';'pitch'};
sys.InputName = {'left wheel torque';'right wheel torque'};

%% Transformation matrix 
T = [ 0  0 -w/r 0;
    -2/r 0   0  0;
      0  1   0  0;
      0  0   0  1];

sys = ss2ss(sys, T);

Kvec = [-0.027554553120102,  -0.094404765852304,   1.037419862608175,...
         6.560526190967896,  0.000221077211128,   0.000154323799051];

% LQ controller used in real robot:
Klqr = [-Kvec(1), -Kvec(2), -Kvec(3), -Kvec(4), -Kvec(5), -Kvec(6);
        Kvec(1), -Kvec(2), -Kvec(3), -Kvec(4), Kvec(5), -Kvec(6)];

%% Discretization
[Mat, Delta, blkstruct] = lftdata(sys);

Matd = c2d(Mat, h);
sysd = lft(Delta, Matd);


%% Controller design
L = [1, 0, 0, 0;
     0, 1, 0, 0];

Aint = [sysd.A, zeros(4,2);
         -L,     eye(2)];

Bint = [ sysd.B;
        zeros(2)];

Ai_nom = usubs(Aint,'l',0.32336); %.324
Bi_nom = usubs(Bint,'l',0.32336); %.324

%% Optional: H2-optimal feedback
% Q = diag([15,5,1,1,3e-3,3e-3]);
% R = diag([50000, 50000]);
% 
% A = Ai_nom;
% B2 = Bi_nom;
% 
% [nx,nu] = size(B2);
% 
% B1 = B2;
% 
% C1 = [sqrt(Q);zeros(2,6)];
% 
% D12 = [ zeros(6,2);
%          sqrt(R)];
% 
% [~, nw] = size(B1);
% 
% cvx_begin sdp
%     variable upsilon
%     variable P(nx, nx) symmetric
%     variable F(nu, nx)
%     variable Z(nx+2, nx+2) symmetric
%     minimize(upsilon)
%     [      P       , A*P - B2*F,      B1;
%      P'*A' - F'*B2',     P     , zeros(nx,nw);
%           B1'     , zeros(nx,nw)',  eye(nw) ] > 0;
% 
%     [       Z        ,  C1*P - D12*F;
%      P'*C1' - F'*D12',       P       ] > 0;
% 
%     trace(Z) < upsilon;
% cvx_end
% 
% K = -F/P

%% JFAP

% Design own controller
Q = diag([15,5,1,1,2e-3,2e-3]);
R = diag([50000, 50000]);
[Knom, S, ~] = dlqr(Ai_nom, Bi_nom, Q, R);

%Knom = -Klqr; % Design for controller already used in the robot

Acl = Ai_nom-Bi_nom*(Knom);

% Jordan form of closed loop system
[V,J] = eig(Acl);
[V,J] = cdf2rdf(V,J);

H = real(Knom*V);

lp = linspace(l.range(1), l.range(2), 20);
nlp = length(lp);

Br = [zeros(4,2); eye(2)];

[ni,mi] = size(Br);

Klyap = zeros(mi,ni,nlp);

syscl = ss([]);

for i=1:nlp
    A = usubs(Aint,'l',lp(i));
    B = usubs(Bint,'l',lp(i));

    X = lyap(A,-J,B*H);

    Ki = -H/X;

    Klyap(:,:,i) = Ki;

    Aint = [sysd.A, zeros(4,2);
            -L,     eye(2)];

    Bint = [ sysd.B;
            zeros(2)];

    A = usubs(Aint,'l',lp(i));
    B = usubs(Bint,'l',lp(i));

    Aint = [sysd.A, zeros(4,2);
            -L,     eye(2)];

    Bint = [ sysd.B;
            zeros(2)];

    syscl(:,:,i) = ss(A-B*Ki, Br, eye(4,6),[],h);
end

s = 2;

Tinv = [
        -w/r, 0, 0, 0, 0, 0;
        0, -2/r, 0, 0, 0, 0;
        0, 0, 1, 0, 0, 0;
        0, 0, 0, 1, 0, 0;
        0, 0, 0, 0, -w/r, 0;
        0, 0, 0, 0, 0, -2/r;
      ];


if ~real_robot %Transform back to simulator
    for i=1:nlp
        Klyap(:,:,i) = Klyap(:,:,i)*Tinv;
    end
end

% gains fitting
coeff = zeros(mi,ni,s+1);
for i=1:ni
    for j=1:mi
        coeff(j,i,:) = polyfit(lp, squeeze(Klyap(j,i,:)), s);
    end
end

c1 = squeeze(coeff(:,2,:));
c2 = squeeze(coeff(:,3,:));
c3 = squeeze(coeff(:,1,:));
c4 = squeeze(coeff(:,4,:));
c5 = squeeze(coeff(:,5,:));
c6 = squeeze(coeff(:,6,:));

c1r = squeeze(coeff(:,1,:));
c2r = squeeze(coeff(:,2,:));
c3r = squeeze(coeff(:,3,:));
c4r = squeeze(coeff(:,4,:));
c5r = squeeze(coeff(:,5,:));
c6r = squeeze(coeff(:,6,:));

%% Show the result
s1 = ['c1: ', num2str(c1r(1,3))];
s2 = ['c2: ', num2str(c2r(1,1)), ', ', num2str(c2r(1,2)), ', ', num2str(c2r(1,3))];
s3 = ['c3: ', num2str(c3r(1,1)), ', ', num2str(c3r(1,2)), ', ', num2str(c3r(1,3))];
s4 = ['c4: ', num2str(c4r(1,1)), ', ', num2str(c4r(1,2)), ', ', num2str(c4r(1,3))];
s5 = ['c5: ', num2str(c5r(1,3))];
s6 = ['c6: ', num2str(c6r(1,1)), ', ', num2str(c6r(1,2)), ', ', num2str(c6r(1,3))];

disp(s1);
disp(s2);
disp(s3);
disp(s4);
disp(s5);
disp(s6);

c = [c1(1,:); c2(1,:); c3(1,:); c4(1,:); c5(1,:); c6(1,:)];

if real_robot == 0
    save('coeffK.mat', 'c');
end

%% Plot the gains
figure;
subplot(2,3,1)
plot(lp, squeeze(Klyap(:,2,:)),'LineWidth',2)
xlabel('$\rho$[m]', 'Interpreter', 'Latex', 'FontSize',12)
ylabel('$K_{11}, K_{21}$', 'Interpreter', 'Latex', 'FontSize',12)
grid minor
% hold on
% plot(lp, c1(:,1)*lp.^2+c1(:,2)*lp+c1(:,3),'LineWidth',2,'LineStyle','--')

subplot(2,3,2)
plot(lp, squeeze(Klyap(:,3,:)),'LineWidth',2)
xlabel('$\rho$[m]', 'Interpreter', 'Latex', 'FontSize',12)
ylabel('$K_{12}, K_{22}$', 'Interpreter', 'Latex', 'FontSize',12)
grid minor
% hold on
% plot(lp, c2(:,1)*lp.^2+c2(:,2)*lp+c2(:,3),'LineWidth',2,'LineStyle','--')

subplot(2,3,3)
plot(lp, squeeze(Klyap(:,1,:)),'LineWidth',2)
xlabel('$\rho$[m]', 'Interpreter', 'Latex', 'FontSize',12)
ylabel('$K_{13}, K_{23}$', 'Interpreter', 'Latex', 'FontSize',12)
grid minor
% hold on
% plot(lp, c3(:,1)*lp.^2+c3(:,2)*lp+c3(:,3),'LineWidth',2,'LineStyle','--')

subplot(2,3,4)
plot(lp, squeeze(Klyap(:,4,:)),'LineWidth',2)
xlabel('$\rho$[m]', 'Interpreter', 'Latex', 'FontSize',12)
ylabel('$K_{14}, K_{24}$', 'Interpreter', 'Latex', 'FontSize',12)
grid minor
% hold on
% plot(lp, c4(:,1)*lp.^2+c4(:,2)*lp+c4(:,3),'LineWidth',2,'LineStyle','--')

subplot(2,3,5)
plot(lp, squeeze(Klyap(:,5,:)),'LineWidth',2)
xlabel('$\rho$[m]', 'Interpreter', 'Latex', 'FontSize',12)
ylabel('$K_{15}, K_{25}$', 'Interpreter', 'Latex', 'FontSize',12)
grid minor
% hold on
% plot(lp, c5(:,1)*lp.^2+c5(:,2)*lp+c5(:,3),'LineWidth',2,'LineStyle','--')

subplot(2,3,6)
plot(lp, squeeze(Klyap(:,6,:)),'LineWidth',2)
xlabel('$\rho$[m]', 'Interpreter', 'Latex', 'FontSize',12)
ylabel('$K_{16}, K_{26}$', 'Interpreter', 'Latex', 'FontSize',12)
grid minor

pos = get(gcf, 'Position');
set(gcf, 'Position',pos+[-100 0 100 0])
% hold on
% plot(lp, c6(:,1)*lp.^2+c6(:,2)*lp+c6(:,3),'LineWidth',2,'LineStyle','--')


%% CL System response
figure;
step(syscl);
grid minor

time = 0:h:4;
out = zeros(length(time), 4, 2, nlp);

for i=1:nlp
    [out(:,:,:,i), ~] = step(syscl(:,:,i), time);
end

nld = nlp;

figure;
subplot(4,1,1)
for i=1:nlp
    plot(time, out(:, 1, 1, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor
ylabel('$\dot{\psi}$[rad/s]', 'Interpreter', 'Latex')
xlabel('$t$[s]', 'Interpreter', 'Latex');
hold off

subplot(4,1,2)
for i=1:nlp
    plot(time, out(:, 2, 1, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor
ylabel('$\dot{x}$[m/s]', 'Interpreter', 'Latex')
xlabel('$t$[s]', 'Interpreter', 'Latex');
hold off

subplot(4,1,3)
for i=1:nlp
    plot(time, out(:, 3, 1, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor
ylabel('$\dot{\phi}$[rad/s]', 'Interpreter', 'Latex')
xlabel('$t$[s]', 'Interpreter', 'Latex');
hold off

subplot(4,1,4)
for i=1:nlp
    plot(time, out(:, 4, 1, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor
ylabel('$\phi$[rad]', 'Interpreter', 'Latex')
xlabel('$t$[s]', 'Interpreter', 'Latex');
hold off

figure;
subplot(4,2,1)
for i=1:nld
    plot(time, out(:, 1, 1, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor;
ylabel('$\dot{\psi}$[rad/s]', 'Interpreter', 'Latex', 'FontSize',12)
xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
hold off

subplot(4,2,2)
for i=1:nld
    plot(time, out(:, 1, 2, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor;
ylabel('$\dot{\psi}$[rad/s]', 'Interpreter', 'Latex', 'FontSize',12)
xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
hold off

subplot(4,2,3)
for i=1:nld
    plot(time, out(:, 2, 1, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor;
ylabel('$\dot{x}$[m//s]', 'Interpreter', 'Latex', 'FontSize',12)
xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
hold off

subplot(4,2,4)
for i=1:nld
    plot(time, out(:, 2, 2, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor;
ylabel('$\dot{x}$[m/s]', 'Interpreter', 'Latex', 'FontSize',12)
xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
hold off

subplot(4,2,5)
for i=1:nld
    plot(time, out(:, 3, 1, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor;
ylabel('$\dot{\phi}$[rad/s]', 'Interpreter', 'Latex', 'FontSize',12)
xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
hold off

subplot(4,2,6)
for i=1:nld
    plot(time, out(:, 3, 2, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor;
ylabel('$\dot{\phi}$[rad/s]', 'Interpreter', 'Latex', 'FontSize',12)
xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
hold off

subplot(4,2,7)
for i=1:nld
    plot(time, out(:, 4, 1, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor;
ylabel('$\phi$[rad]', 'Interpreter', 'Latex', 'FontSize',12)
xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
hold off

subplot(4,2,8)
for i=1:nld
    plot(time, out(:, 4, 2, i), 'LineWidth', 2, 'Color', "#0072BD");
    hold on
end
grid minor;
ylabel('$\phi$[rad]', 'Interpreter', 'Latex', 'FontSize',12)
xlabel('$t$[s]', 'Interpreter', 'Latex', 'FontSize',12);
hold off

pos = get(gcf, 'Position');
set(gcf, 'Position',pos+[0 -50 0 50])


% %% H∞ norm
% cvx_begin sdp
%     variable rho
%     variable P(n,n) symmetric
%     minimize (rho)
%     P > 0  
%     [ A'*P + P*A + C'*C P*B; ...
%         B'*P -rho*eye(m)] < 0
% cvx_end
% gamma = sqrt(rho);


