using RobustAndOptimalControl
using LinearAlgebra
using ControlSystems
using Plots

# Close all; clear; clc equivalent

# Parameters
w = 0.29 # wheel base
l = 0.33226 # Rod length (uncertainty can be added with another package)
r = 0.08 # Wheel diameter
m_b = 4.0 # Body mass
m_w = 0.3 # Wheel mass
J = 0.000735 # Wheel moment of inertia about its turning axis
K = 0.00039 # Wheel moment of inertia 
I_x = 0.0484 # Pendulum moment of inertia about X (roll)
I_y = 0.0377 # Pendulum moment of inertia about Y (pitch)
I_z = 0.0406 # Pendulum moment of inertia about Z (yaw)
b = 0.01 # Wheel damping
g = 9.81

h = 0.001


# Linearized 3D Segway Model (already reduced...)
d1 = (2 * I_y * J + I_y * m_b * r^2 + 2 * I_y * m_w * r^2 + 2 * J * l^2 * m_b + 2 * l^2 * m_b * m_w * r^2)

d2 = (r * (2 * I_y * J + I_y * m_b * r^2 + 2 * I_y * m_w * r^2 + 2 * J * l^2 * m_b + 2 * l^2 * m_b * m_w * r^2))

d3 = (4 * K * r^2 + 2 * I_z * r^2 + J * w^2 + m_w * r^2 * w^2)

a11 = -(2 * I_y * b + 2 * b * l * m_b * r + 2 * b * l^2 * m_b) / d1
a12 = (2 * I_y * b * r + 2 * b * l * m_b * r^2 + 2 * b * l^2 * m_b * r) / d1
a14 = -(g * l^2 * m_b^2 * r^2) / d1

a21 = (4 * J * b + 2 * b * m_b * r^2 + 4 * b * m_w * r^2 + 2 * b * l * m_b * r) / d2
a22 = -(4 * J * b * r + 2 * b * m_b * r^3 + 4 * b * m_w * r^3 + 2 * b * l * m_b * r^2) / d2
a24 = (2 * J * g * l * m_b * r + g * l * m_b^2 * r^3 + 2 * g * l * m_b * m_w * r^3) / d2

a33 = -(b * w^2) / d3

Ac = [a11 a12 0 a14;
      a21 a22 0 a24;
      0   0   a33 0;
      0   1   0   0]

b11 = -(I_y * r + l * m_b * r^2 + l^2 * m_b * r) / d1
b12 = b11

b21 = (2 * J * r + m_b * r^3 + 2 * m_w * r^3 + l * m_b * r^2) / d2
b22 = b21

b31 = (r * w) / d3
b32 = -b31

Bc = [b11 b12;
      b21 b22;
      b31 b32;
      0   0]

Cc = I(size(Ac, 1))
Dc = zeros(size(Bc, 1), size(Bc, 2))

G = ss(Ac, Bc, Cc, Dc)

Gd = c2d(G, h, :zoh)

s = tf("s")

w = 1.1*(s + 1.2)/s

wd = c2d(w, h, :zoh)

W1 = wd.*I(2)


K1dof, γ1, info1 = glover_mcfarlane(Gd, 1.1; W1)

println("γ1: ", γ1)

L = minreal(Gd * (K1dof))

#τ₁ = 0.5
#τ₂ = 0.4

#R = (τ₁*s + 1)/(τ₂*s + 1)

τ₁ = 0.01
τ₂ = 0.15

R = 15/(s+15)#(τ₁*s + 1)/(τ₂*s + 1)

R = c2d(R, h, :zoh)

S = minreal(feedback(I(4), L))

T = minreal(I(4) - S)

fig1 = plot(step(T[1,1], 0:0.001:10))  
xlims!(0, 4) 

fig2 = plot(step(R*T[3, 3], 0:0.001:10)) 
xlims!(0, 4)

plot(fig1,fig2)