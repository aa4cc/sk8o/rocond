%% Clear All
clear; clc; close all;
%% Script variables
real_v_simscape = 0;
simulator = 1;
simscape_v_segway = 0;

%% Parameters
d = 0.29;
l.b = 0.1; %m translation from COG to level of first joint in z axis
l.w = 0.0925; %m half of body length in y coordinate
l.u = 0.188; %m length of upper leg
l.lk = 0.05; %m length of lower leg from knee to end 
l.k = 0.193; %m length of kinematic loop leg
l.l = 0.19; %m length of lower leg
l.o = 0.026; %m length to wheel
l.com = 0.08; %m 
l.ml = l.lk - l.com; %m
l.uk = 0.098; %m
m.b = 3; %kg body weight
m.u = 0.2; %kg upper leg weight
m.l = 0.2; %kg lower leg weight
m.k = 0.1; %kg kinematic loop weight
m.w = 0.3; %kg wheel weight
r = 0.08; %m wheel radius
I.mb = diag([4.84e-2, 4.06e-2, 3.77e-2]);%diag([6.57e-2 6.57e-2 9.25e-2]);%diag([6.57e-2 6.57e-2 9.25e-2]); % body moment of inertia
I.mu = diag([6.04e-4 1.67e-5 5.91e-4]); % upper leg moment of inertia
I.ml = diag([9.75e-4 1.67e-5 9.62e-4]); % lower leg moment of inertia
I.mk = diag([6.36e-4 1.67e-5 6.22e-4]); % kinematic loop moment of inertia
I.mw = diag([3.90e-4 3.90e-4 7.35e-4]); % wheel moment of inertia
g = 9.81; %m/s^2 gravity force
%% Initial Configuration
% Upper Leg
upper_leg.joint.ref = 43.8; %deg
upper_leg.joint.min = 18; %deg
upper_leg.joint.max = 60; %deg
upper_leg.joint.damping = 0.5;

% Lower Leg
lower_leg.joint.ref = 92.4; %deg
lower_leg.joint.min = 40; %deg
lower_leg.joint.max = 130; %deg
lower_leg.joint.stiffness = 0.247;
lower_leg.joint.springref = -240; %deg
lower_leg.joint.damping = 0.3;
lower_leg.joint.frictionloss = 0.1;

% Leg Link (Kinematic Loop)
leg_link.joint.ref = 100.5; %deg

% Wheel
wheel.b = .01;

%% Design LQR

Kvec = [-0.027554553120102,  -0.094404765852304,   1.037419862608175,...
      6.560526190967896,  0.000221077211128,   0.000154323799051];

% LQ controller used in real robot:
Klqr = [-Kvec(1), -Kvec(2), -Kvec(3), -Kvec(4), -Kvec(5), -Kvec(6);
        Kvec(1), -Kvec(2), -Kvec(3), -Kvec(4), Kvec(5), -Kvec(6)];

% Transformation matrix for transforming real robot states to sim states 
T =    [
        -d/r, 0, 0, 0,   0,   0;
        0, -2/r, 0, 0,   0,   0;
        0,   0,  1, 0,   0,   0;
        0,   0,  0, 1,   0,   0;
        0,   0,  0, 0, -d/r,  0;
        0,   0,  0, 0,   0, -2/r;
       ];

% T = [
%         r/d, 0, 0, 0, 0, 0;
%         0, r/2, 0, 0, 0, 0;
%         0, 0, 1, 0, 0, 0;
%         0, 0, 0, 1, 0, 0;
%         0, 0, 0, 0, r/d, 0;
%         0, 0, 0, 0, 0, r/2;
%     ];

Klqr = Klqr*T;

%% STL to pointcloud
% body_black_pts = stlread('stl/body_black.stl').Points;
% body_blue_pts = stlread('stl/body_blue.stl').Points;
% rul_pts = stlread('stl/right_upper_leg.stl').Points;
% lul_pts = stlread('stl/left_upper_leg.stl').Points;
% rkl_pts = stlread('stl/right_leg_link.stl').Points;
% lkl_pts = stlread('stl/left_leg_link.stl').Points;
% rll_pts = stlread('stl/right_lower_leg.stl').Points;
% lll_pts = stlread('stl/left_lower_leg.stl').Points;
% rw_rim_pts = stlread('stl/right_rim.stl').Points;
% lw_rim_pts = stlread('stl/left_rim.stl').Points;
% ltire_pts = stlread('stl/left_tire.stl').Points;
% rtire_pts = stlread('stl/right_tire.stl').Points;

%% Ground Surface Generation
% V = 1:500;
% B = 0.01*(sin(0.13*V.')*V);
% B = B*0;

%% Parameters for contacts
h = 0.001; % Sampling period
contact_force.k = 1e6;
contact_force.b = 1e3;
contact_force.tw = 1e-4;

static_friction = .5;
dynamic_friction = .3;
critical_velocity = 1e-4;

%% Load controllers
%controller = load('Kinfsyn.mat');
K2dof = load('Kdof.mat');
state_controller = load('Klqr.mat');
%lyap_controller = load('Klyap.mat');
coeff_controller = load('coeffK.mat');
%KHinf = load('KHinf.mat');
%KH2 = load('KH2.mat');
K0 = load('K0.mat');
Kncf = load('Kls.mat');
prefW = load('W1.mat');
postW = load('W2.mat');
% Robust
Kstab = load('Kstab.mat');
Kperf = load('Kperf.mat');

nominal_system = load('nominal_system.mat');
%K2 = K2dof.K2;
%Kinf = controller.Kinf;
Kx = state_controller.Kx;
%Klyap = lyap_controller.Klyap;
coeff = coeff_controller.c;
%KHinf = KHinf.K;
%KH2 = KH2.K;
K0 = K0.K0;

W1 = prefW.W1z;
W2 = postW.W2;
Kls = Kncf.Kz;

Kp = Kperf.Kp;
Ks = Kstab.Ks;

sysd = nominal_system.sysd;

Kpend = Klqr;


%% Prepare measurement from real robot
t_max = 20;
t_max_ref = t_max + 1;

cmp = load('data/segway_data.mat');
cmp = cmp.data;
t = 0:h:t_max;
t_ref = 0:h:t_max_ref;

dx = zeros(size(t));
dyaw = zeros(size(t));
dot_x = zeros(size(t));
dot_psi = zeros(size(t));
dot_phi = zeros(size(t));
phi = zeros(size(t));
k = 1;

for i = 1:length(t_ref)
    if cmp.dot_x_ref.t(k) < t_ref(i) && k < length(cmp.dot_x_ref.t)
        k = k + 1;
    end
    dx(i) = cmp.dot_x_ref.values(k);
    dyaw(i) = cmp.dot_psi_ref.values(k);
end

for i = 1:length(t)
    if cmp.dot_x.t(k) < t(i) && k < length(cmp.dot_x.t)
        k = k + 1;
    end
    dot_x(i) = cmp.dot_x.values(k);
    dot_psi(i) = cmp.dot_psi.values(k);
    dot_phi(i) = cmp.dot_phi.values(k);
    phi(i) = cmp.phi.values(k);
end

%% Run the simulation
load_system('sk8o')
set_param('sk8o','SimMechanicsOpenEditorOnUpdate','on');
out = sim('sk8o', 'StopTime', string(t_max));

if simulator
%% Velocity plot
figure;
% subplot(4, 1, 1)
plot(out.dx.Time(3000:end)-3, out.dx.Data(3000:end), 'LineWidth',2)
hold on
plot(out.dx_ref.Time(3000:end)-3, out.dx_ref.Data(3000:end), 'LineWidth',2)
% plot(t_ref, dx, 'LineWidth', 2)
legend('Response', 'Reference', 'Interpreter','latex');
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\dot{x}$[rad/s]', 'Interpreter','latex');
grid on
grid minor
xlim([0, t_max-3]);

%% YAW Rate plot
figure;
% subplot(4, 1, 2)
plot(out.dyaw.Time(3000:end)-3, out.dyaw.Data(3000:end), 'LineWidth',2)
hold on
plot(out.dyaw_ref.Time(3000:end)-3, out.dyaw_ref.Data(3000:end), 'LineWidth',2)
% plot(t_ref, dyaw, 'LineWidth', 2)
legend('Response', 'Reference','Interpreter','latex');
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\dot{\psi}$[rad/s]', 'Interpreter','latex');
grid on
grid minor
xlim([0, t_max-3]);

%% Pitch angle plot
figure;
% subplot(4, 1, 3)
% plot(cmp.phi.t, cmp.phi.values, 'LineWidth',2)
% hold on
plot(out.pitch.Time(3000:end)-3, out.pitch.Data(3000:end), 'LineWidth',2)
legend('Response', 'Interpreter','latex');
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\theta$[rad]', 'Interpreter','latex');
grid on
grid minor
xlim([0, t_max-3]);

%% Pitch rate plot
figure;
%subplot(4, 1, 4)
plot(out.dpitch.Time(3000:end)-3, out.dpitch.Data(3000:end), 'LineWidth',2)
legend('Response', 'Interpreter','latex');
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\dot{\theta}$[rad/s]', 'Interpreter','latex');
grid on
grid minor
xlim([0, t_max-3]);
end

data.time = out.dx.Time(3000:end)-3;
data.dx = out.dx.Data(3000:end);
data.dx_ref = out.dx_ref.Data(3000:end);
data.dyaw = out.dyaw.Data(3000:end);
data.dyaw_ref = out.dyaw_ref.Data(3000:end);
data.dpitch = out.dpitch.Data(3000:end);
data.pitch = out.pitch.Data(3000:end);
data.uL = out.uL.Data(3000:end);
data.uR = out.uR.Data(3000:end);
data.hipL = out.hipL.Data(3000:end);
data.hipR = out.hipR.Data(3000:end);
data.hipLr = out.hipLr.Data(3000:end);
data.hipRr = out.hipRr.Data(3000:end);


if real_v_simscape
%% Velocity plot
figure;
% subplot(4, 1, 1)
plot(cmp.dot_x.t, cmp.dot_x.values, 'LineWidth',2)
hold on
plot(out.dx.Time, out.dx.Data, 'LineWidth',2)
hold on
plot(t_ref, dx, 'LineWidth', 2)
legend('Real robot', 'Simulation', 'Reference', 'Interpreter','latex');
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\dot{x}$[rad/s]', 'Interpreter','latex');
grid on
grid minor
xlim([0, t_max]);

%% YAW Rate plot
figure;
% subplot(4, 1, 2)
plot(cmp.dot_psi.t, cmp.dot_psi.values, 'LineWidth',2)
hold on
plot(out.dyaw.Time, out.dyaw.Data, 'LineWidth',2)
hold on
plot(t_ref, dyaw, 'LineWidth', 2)
legend('Real robot', 'Simulation', 'Reference','Interpreter','latex');
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\dot{\psi}$[rad/s]', 'Interpreter','latex');
grid on
grid minor
xlim([0, t_max]);

%% Pitch angle plot
figure;
% subplot(4, 1, 3)
plot(cmp.phi.t, cmp.phi.values, 'LineWidth',2)
hold on
plot(out.pitch.Time, out.pitch.Data, 'LineWidth',2)
legend('Real robot', 'Simulation', 'Interpreter','latex');
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\theta$[rad]', 'Interpreter','latex');
grid on
grid minor
xlim([0, t_max]);

%% Pitch rate plot
figure;
%subplot(4, 1, 4)
plot(cmp.dot_phi.t, cmp.dot_phi.values, 'LineWidth',2)
hold on
plot(out.dpitch.Time, out.dpitch.Data, 'LineWidth',2)
legend('Real robot', 'Simulation', 'Interpreter','latex');
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\dot{\theta}$[rad/s]', 'Interpreter','latex');
grid on
grid minor
xlim([0, t_max]);
end
%% Comparison with 3D segway model

if simscape_v_segway
tspan = 0:h:t_max;

u = @(t, x, eps) Klqr*[x; eps];

x0 = [0; 0; 0; 0; -deg2rad(5); 0; 0; 0];

ref = [0; 0];

Y = ode5(@(t ,x) segway_model(t, x, u, ref), tspan, x0);

% Velocity comparison
figure;
plot(out.dx.Time, out.dx.Data, 'LineWidth', 2)
hold on
plot(tspan, Y(:, 1), 'LineWidth', 2)
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\dot{x}$[rad/s]', 'Interpreter','latex');


% Yaw rate comparison
figure;
plot(out.dyaw.Time, out.dyaw.Data, 'LineWidth',2)
hold on
plot(tspan, Y(:, 3), 'LineWidth', 2)
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\dot{\psi}$[rad/s]', 'Interpreter','latex');

% Pitch rate comparison
figure;
plot(out.dpitch.Time, out.dpitch.Data, 'LineWidth',2)
hold on
plot(tspan, Y(:, 2), 'LineWidth', 2)
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\dot{\theta}$[rad/s]', 'Interpreter','latex');

% Pitch angle comparison
figure;
plot(out.pitch.Time, out.pitch.Data, 'LineWidth',2)
hold on
plot(tspan, Y(:, 5), 'LineWidth', 2)
xlabel('$t$[s]', 'Interpreter','latex');
ylabel('$\theta$[rad]', 'Interpreter','latex');
end



