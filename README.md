# Robust control design for Sk8o robot
This repository containts the full rigid body model of two wheeled-legged robot Sk8o together with controllers and some data.

## Sk8o robot
The robot has two legs with a closed kinematic chain ending with actuated
wheels. These legs are attached to the body at the hip. In the body, all the
electronics, except motors, are concerned. Each leg can be extended and stretched
independently by controlling the corresponding motor installed in the hip. A
rapid extension of both legs allows the robot to jump. Moreover, the joint is
situated between the hip and wheels, referred to as the knee. The knees contain a
torsion spring that partially counteracts gravity. Most of the parts were created
using a 3D printer.

## Structure of the simulator
The entire model is developed in the MATLAB Simscape environment and is divided into two main files. The first file, sk8o_parameters.m, is a script that primarily defines the parameters of the robot. The second file is a Simulink schematic that interacts with the Simscape model, representing the Sk8o robot. In Simulink, the control structure is implemented, featuring two types of controllers.

The first controller is a PD controller, which is used to control the angle at the hips, thereby extending the legs. The second controller is a wheel controller, designed to maintain balance and follow reference trajectories.

## Instructions to run the simulator
To run the simulator, you must first set the appropriate paths. In MATLAB, navigate to the folder containing the *rocond* folder and type in the MATLAB command window:
```addpath(genpath([pwd filesep 'rocond']))```.
Next, run the script *sk8o_parameters.m*. Note that it is essential to run this script before attempting to run sk8o.slx, as the Simulink model relies on the parameters defined in the script.

